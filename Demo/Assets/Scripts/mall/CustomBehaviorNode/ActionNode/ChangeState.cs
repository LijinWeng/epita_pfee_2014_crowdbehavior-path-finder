﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class ChangeState : ActionNode
{

		//public GameObject gameObject;
		public ShopperBehavior.State state;

		// Called when the node will be ticked
		public override void OnTick ()
		{
        
				ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal> () as ShopperGlobal;

				if (shopperEntity != null) {
						shopperEntity.behaviorEntity.state = this.state;

						// Will be used later for reorientation when going back to idle state
						if (this.state != ShopperBehavior.State.IDLE)
								shopperEntity.idleBehaviorEntity.lastNodeInIdleState = shopperEntity.idleBehaviorEntity.currentNode;

                        if (this.state != ShopperBehavior.State.STEAL)
                            shopperEntity.actionBehaviorEntity.shopperToSteal = null;
	
						// Change color depending on state
						Renderer[] rendererList = self.GetComponentsInChildren<Renderer> ();
						foreach (Renderer render in rendererList)
								render.material = GlobalEntities.stateMaterials [(int)this.state];

						// Set state also in blackboard
						blackboard.GetIntVar ("State").Value = (int)state;

						status = Status.Success;
				} else
						status = Status.Error;
        
		}
}
