﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class ChangeMaterial : ActionNode
{

    //public GameObject gameObject;
    public Material material;

    // Called when the node will be ticked
    public override void OnTick()
    {
        Renderer[] rendererList = self.GetComponentsInChildren<Renderer>();

        foreach (Renderer render in rendererList)
        {
            render.material = this.material;
        }

        status = Status.Success;
    }
}
