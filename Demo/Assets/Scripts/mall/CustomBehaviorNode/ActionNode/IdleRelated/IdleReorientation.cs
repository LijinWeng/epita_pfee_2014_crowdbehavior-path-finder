﻿
using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class IdleReorientation : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {

        ShopperIdle idleEntity = self.GetComponent<ShopperIdle>() as ShopperIdle;
        ShopperNavigation navigationEntity = self.GetComponent<ShopperNavigation>() as ShopperNavigation;

        if (idleEntity.currentNode != idleEntity.lastNodeInIdleState)
        {
            if (idleEntity.wantToExit == true)
            {
                // If not arrived at exit
                if (idleEntity.currentNode.nextNodeToExit)
                    idleEntity.targetNode = idleEntity.currentNode.nextNodeToExit;

            }
            // Not arrived to current goal
            else if (idleEntity.currentTargetShopNode)
            {
                idleEntity.nodeListToCurrentTargetShop = AStar.GetPath(idleEntity.currentNode, idleEntity.currentTargetShopNode);
                idleEntity.targetNode = idleEntity.nodeListToCurrentTargetShop[0];
            }

        }

        navigationEntity.TargetPosition = idleEntity.targetNode.transform.position;
        status = Status.Success;
    }
}
