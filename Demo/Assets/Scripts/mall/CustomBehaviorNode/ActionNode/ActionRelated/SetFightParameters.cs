﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class SetFightParameters : ActionNode {
	
	public override void OnTick() {

        ShopperGlobal global = self.GetComponent<ShopperGlobal>();
        ShopperAction action = global.actionBehaviorEntity;
        ShopperBehavior behavior = global.behaviorEntity;
		
		if (behavior.state == ShopperBehavior.State.FIGHT_GOOD) {
			++action.fightArea.goodSum;
			action.fightThreshold = action.fightArea.goodSum * action.fightArea.threshold;
		} else {
			++action.fightArea.badSum;
			action.fightThreshold = -1f * action.fightArea.badSum * action.fightArea.threshold;
		}

        global.SetAnimToFight();
	}
}