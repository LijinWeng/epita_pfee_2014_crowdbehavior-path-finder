﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class HaveChaseTarget : ConditionNode {

    // Called when the node will be ticked
    public override void OnTick () {

        ShopperAction actionEntity = self.GetComponent<ShopperAction>();

        if (actionEntity.shopperToChase != null)
            status = Status.Success;
        else
            status = Status.Failure;

    }
}
