﻿
using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsEnteredInShop : ConditionNode {

    public GameObjectVar gameObject;

    // Called when the node will be ticked
    public override void OnTick () {
        GameObject unityGameObj = (GameObject)gameObject;
        Shop shopDoor = unityGameObj.GetComponent<Shop>() as Shop;
        if (shopDoor != null)
            status = Status.Success;
        else
            status = Status.Failure;
    }
}
