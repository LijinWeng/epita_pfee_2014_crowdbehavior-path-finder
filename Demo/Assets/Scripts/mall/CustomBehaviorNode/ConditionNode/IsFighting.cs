﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsFighting : ConditionNode
{

    
    // Called when the node will be ticked
    public override void OnTick()
    {
        status = Status.Failure;
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        if (shopperEntity != null)
        {
            if (shopperEntity.behaviorEntity.state >= ShopperBehavior.State.FIGHT_GOOD)
                status = Status.Success;
        }

    }
}
