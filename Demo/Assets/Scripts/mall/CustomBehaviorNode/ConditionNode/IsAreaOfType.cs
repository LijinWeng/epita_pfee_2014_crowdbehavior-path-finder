﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class IsAreaOfType : ConditionNode {

    public GameObjectVar gameObject;
    public Area.AreaType typeOfArea;

    // Called when the node will be ticked
    public override void OnTick () {
        GameObject unityGameObj = (GameObject)gameObject;
        Area correspodingNode = unityGameObj.GetComponent<Area>();

        if (correspodingNode != null && correspodingNode.areaType == typeOfArea)
            status = Status.Success;
        else
            status = Status.Failure;
    }
}
