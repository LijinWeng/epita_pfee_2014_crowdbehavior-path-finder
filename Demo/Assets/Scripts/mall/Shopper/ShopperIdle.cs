﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopperIdle : MonoBehaviour
{

    public List<NavNode> shopNodeList = new List<NavNode>();
    public List<Vector3> shopPositionList = new List<Vector3>();

    public List<NavNode> nodeListToCurrentTargetShop = new List<NavNode>();

    public NavNode currentTargetShopNode;
    public NavNode targetNode;
    public NavNode currentNode;

    // Used for reorientation
    public NavNode lastNodeInIdleState = null;

    public bool wantToExit = false;


    public void PickUpShop()
    {
        shopNodeList.Clear();
        shopPositionList.Clear();

        int numberOfShopToGo = Random.Range(3, 7);
        List<GameObject> shopList = new List<GameObject>(GlobalEntities.doorShopList);

        for (int i = 0; i < numberOfShopToGo; i++)
        {
            // Pick a random shop
            int shopIdx = Random.Range(0, shopList.Count);
            GameObject currentShop = shopList[shopIdx];
            shopList.RemoveAt(shopIdx);

            // Find the door
            Shop doorShop = currentShop.GetComponent(typeof(Shop)) as Shop;

            // Take its location and its corresponding node then adding to goalLists
            shopNodeList.Add(doorShop.entryNode);
            shopPositionList.Add(doorShop.transform.position);
        }

        currentTargetShopNode = shopNodeList[0];
        ComputeAStarPath();
    }


    public void ComputeAStarPath()
    {
        nodeListToCurrentTargetShop = AStar.GetPath(currentNode, currentTargetShopNode);
        ChangeTargetNode();
    }

    public void ChangeGoalNode()
    {
        shopNodeList.RemoveAt(0);
        shopPositionList.RemoveAt(0);

        if (shopNodeList.Count > 0)
        {
            // Going to a new shop
            currentTargetShopNode = shopNodeList[0];
            targetNode = null;
            ComputeAStarPath();
        }
        else
        {
            // Leaving the mall
            currentTargetShopNode = null;
            nodeListToCurrentTargetShop.Clear();
            wantToExit = true;
            ChangeTargetNode();
        }
    }

    public void ChangeTargetNode()
    {

        if (targetNode)
            currentNode = targetNode;
        else
            targetNode = currentNode;
        //targetNode = nodeListToTargetedShop[0];

        if (wantToExit == true)
        {
            // If not arrived at exit
            if (currentNode.nextNodeToExit)
                targetNode = currentNode.nextNodeToExit;

        }
        // Not arrived to current goal
        else if (nodeListToCurrentTargetShop.Count > 1)
        {
            nodeListToCurrentTargetShop.RemoveAt(0);
            targetNode = nodeListToCurrentTargetShop[0];
        }
        //targetPosition = new Vector3(targetNode.transform.position.x, transform.position.y, targetNode.transform.position.z);
    }

    void UpdateTargetNode()
    {
        if (targetNode != null)
        {
            if (Vector3.Distance(transform.position, targetNode.cachedTransform.position) < targetNode.Radius)
                ChangeTargetNode();
        }
    }
}
