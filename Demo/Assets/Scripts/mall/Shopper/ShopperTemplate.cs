﻿using UnityEngine;
using System.Collections;

public class ShopperTemplate : MonoBehaviour
{
		public RandomDistribution
				SpeedDistribution;
		public RandomDistribution
				StrengthDistribution;
		//public RandomDistribution
			//	BraveryDistribution;
		//public RandomDistribution
			//	ExcitationDistribution;
		public RandomDistribution
				AwarenessDistribution;
		public RandomDistribution
				EagerDistribution;
		//public RandomDistribution
			//	IndependenceDistribution;
		//public RandomDistribution
			//	MoralityDistribution;

		// Use this for initialization
		void Start ()
		{
		}
	
		// Update is called once per frame
		void Update ()
		{
		}

		private float normalRandom ()
		{
				float u1 = Random.value;
				float u2 = Random.value;
				return Mathf.Sqrt (-2.0f * Mathf.Log (u1)) * Mathf.Sin (2.0f * Mathf.PI * u2);
		}
	
		private float normalRandom (float mean, float deviation)
		{
				return mean + deviation * normalRandom ();
		}
}
