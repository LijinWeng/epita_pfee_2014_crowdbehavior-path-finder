﻿using UnityEngine;
using System.Collections;

public class ResetButton : MonoBehaviour {

    public void ResetLevel()
    {
        Application.LoadLevel(0);
    }

    public void SetAllAgentToPanic()
    {
        GameObject[] agentList = GameObject.FindGameObjectsWithTag("Agent");

        foreach (GameObject agent in agentList)
        {
            ShopperGlobal shopperGlobal = agent.GetComponent<ShopperGlobal>();
            shopperGlobal.navigationEntity.speed += 3f;
            shopperGlobal.behaviorEntity.state = ShopperBehavior.State.PANIC;

            // Change color depending on state
            Renderer[] rendererList = shopperGlobal.GetComponentsInChildren<Renderer>();
            foreach (Renderer render in rendererList)
                render.material = GlobalEntities.stateMaterials[(int)ShopperBehavior.State.PANIC];
        }

//		deleteAreas();
    }

    public void ResetAllStatesAgents()
    {
        GameObject[] agentList = GameObject.FindGameObjectsWithTag("Agent");

        foreach (GameObject agent in agentList)
        {

			ShopperTemplate template = agent.GetComponent<ShopperTemplate>();
#if UNITY_EDITOR
			string oldTemplateName = template.name;
#endif
			template = getRandomTemplate(GlobalEntities.shopperGenerator.Templates, GlobalEntities.shopperGenerator.Weights).GetComponent<ShopperTemplate>();

#if UNITY_EDITOR
			agent.name = oldTemplateName.Replace(oldTemplateName.Split('(')[0], template.name);
#endif

            ShopperGlobal shopperEntity = agent.GetComponent<ShopperGlobal>();
            ShopperBehavior.State state = ShopperBehavior.State.IDLE;
            if (shopperEntity != null)
            {
                shopperEntity.behaviorEntity.state = state;

				ShopperNavigation navigationEntity = agent.GetComponent<ShopperNavigation>();

                // Will be used later for reorientation when going back to idle state
                if (state != ShopperBehavior.State.IDLE)
                    shopperEntity.idleBehaviorEntity.lastNodeInIdleState = shopperEntity.idleBehaviorEntity.currentNode;
                else
                {
                    // Idle reorientation
                    ShopperIdle idleEntity = agent.GetComponent<ShopperIdle>();

                    if (idleEntity.currentNode != idleEntity.lastNodeInIdleState)
                    {
                        if (idleEntity.wantToExit == true)
                        {
                            // If not arrived at exit
                            if (idleEntity.currentNode.nextNodeToExit)
                                idleEntity.targetNode = idleEntity.currentNode.nextNodeToExit;

                        }
                        // Not arrived to current goal
                        else if (idleEntity.currentTargetShopNode)
                        {
                            idleEntity.nodeListToCurrentTargetShop = AStar.GetPath(idleEntity.currentNode, idleEntity.currentTargetShopNode);
                            idleEntity.targetNode = idleEntity.nodeListToCurrentTargetShop[0];
                        }

                    }

                    navigationEntity.TargetPosition = idleEntity.targetNode.transform.position;
                }

				// Reset speed
				navigationEntity.speed = template.SpeedDistribution.Evaluate();
				navigationEntity.maxSpeed = navigationEntity.speed * 2;

				// Change shape
				float max = 3f;
				float min = 0f;
				Transform cylinder = agent.transform.FindChild("Animated").FindChild("Cylinder");
				Vector3 vec = cylinder.localScale;
				vec.x = vec.z = (((navigationEntity.speed - min) * (0.8f - 0.4f)) / (max - min)) + 0.4f;
				vec.y = (((navigationEntity.speed - min) * (1.2f - 0.6f)) / (max - min)) + 0.6f;
				cylinder.localScale = vec;
				vec = agent.transform.localScale;
				vec.y = cylinder.localScale.y;
				agent.transform.localScale = vec;

				// Shopper Behavior
				ShopperBehavior agentBehavior = agent.GetComponent<ShopperBehavior>();
				agentBehavior.strength = template.StrengthDistribution.Evaluate();
				//agentBehavior.bravery = template.BraveryDistribution.Evaluate();
				//agentBehavior.excitation = template.ExcitationDistribution.Evaluate();
				agentBehavior.awareness = template.AwarenessDistribution.Evaluate();
				agentBehavior.eager = template.EagerDistribution.Evaluate();
				//agentBehavior.independence = template.IndependenceDistribution.Evaluate();
				//agentBehavior.morality = template.MoralityDistribution.Evaluate();

                // Change color depending on state
                Renderer[] rendererList = agent.GetComponentsInChildren<Renderer>();
                foreach (Renderer render in rendererList)
                    render.material = GlobalEntities.stateMaterials[(int)state];


            }
        }

		deleteAreas();

    }

	private void deleteAreas() {

		GameObject[] areaList = GameObject.FindGameObjectsWithTag("Area");
		
		foreach (GameObject area in areaList) {
			GameObject.Destroy(area);
		}
	}

	private GameObject getRandomTemplate(GameObject[] Templates, float[] Weights)
	{
		float[] cumulativeWeights = new float[Templates.Length];
		cumulativeWeights[0] = Weights[0];
		for (int i = 1; i < Templates.Length; ++i)
		{
			cumulativeWeights[i] = cumulativeWeights[i - 1] + Weights[i];
		}
		
		float r = Random.Range(0, cumulativeWeights[Templates.Length - 1]);
		
		for (int i = 0; i < Templates.Length; ++i)
			if (r <= cumulativeWeights[i])
				return Templates[i];
		
		return null;
	}

}
