﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavNode : MonoBehaviour {

    // Contains list of neighbors nodes
	public List<NavNode> nodeList = new List<NavNode>();

    // List of agent in the node
    public List<ShopperGlobal> agentInNode = new List<ShopperGlobal>();

    public NavNode nextNodeToExit;

    [HideInInspector]
    public Transform cachedTransform;

    float radius = 0f;


    public float Radius
    {
        get { return radius; }
        set { radius = value; }
    }


	// Use this for initialization
	void Start () {
        cachedTransform = this.transform;
		var boxCollider = this.collider as BoxCollider;
		radius = Mathf.Min(boxCollider.size.x, boxCollider.size.z) / 2f;

        SpawnNode[] exitList = GameObject.FindObjectsOfType(typeof(SpawnNode)) as SpawnNode[];
        List<NavNode> pathToExit = new List<NavNode>();
        foreach (SpawnNode exitNode in exitList)
        {
            NavNode exit = exitNode.GetComponentInParent(typeof(NavNode)) as NavNode;
            List<NavNode> tmpList = AStar.GetPath(this, exit);

			if (pathToExit.Count == 0 || pathToExit.Count > tmpList.Count)
            {
                pathToExit = tmpList;
            }
        }

        if (pathToExit.Count > 1)
            nextNodeToExit = pathToExit[1];
        pathToExit.Clear();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        ShopperGlobal agent = other.GetComponent<ShopperGlobal>();
        if (agent != null)
        {
            agentInNode.Add(agent);

            // TODO: modify
            agent.navigationEntity.currentNode = this;
            agent.idleBehaviorEntity.currentNode = this;

        }
    }

    void OnTriggerExit(Collider other)
    {
        ShopperGlobal agent = other.GetComponent<ShopperGlobal>();
        if (agent != null)
        {
            agentInNode.Remove(agent);
        }
    }

    void OnDrawGizmos()
    {
		Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 1f);
        var boxCollider = this.collider as BoxCollider;

        
		foreach (NavNode node in this.nodeList)
        {
            Gizmos.color = Color.green;
            Vector3 halfNodePos = (transform.position + node.transform.position) / 2;
            Gizmos.DrawLine(transform.position, halfNodePos);
        }

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(boxCollider.transform.localPosition, Quaternion.identity, Vector3.one)
            * Matrix4x4.TRS(Vector3.zero, boxCollider.transform.localRotation, Vector3.one)
            * Matrix4x4.TRS(-boxCollider.transform.localPosition, Quaternion.identity, Vector3.one);
        Gizmos.matrix = rotationMatrix;

        Gizmos.DrawWireCube(transform.position, boxCollider.size);
    }

}
