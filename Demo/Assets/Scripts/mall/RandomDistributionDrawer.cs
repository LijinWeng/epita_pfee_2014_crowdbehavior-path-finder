﻿
#if UNITY_EDITOR
using UnityEngine;

using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(RandomDistribution))]


public class RandomDistributionDrawer : PropertyDrawer
{
		// Draw the property inside the given rect
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
				// Using BeginProperty / EndProperty on the parent property means that
				// prefab override logic works on the entire property.
				EditorGUI.BeginProperty (position, label, property);
		
				// Draw label
				position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);
		
				// Don't make child fields be indented
				int indent = EditorGUI.indentLevel;
				EditorGUI.indentLevel = 0;
		
				// Calculate rects
				Rect minValue = new Rect (position.x, position.y, 30, position.height);
				Rect maxValue = new Rect (position.x + 35, position.y, 30, position.height);
				Rect cumulativeDistribution = new Rect (position.x + 70, position.y, position.width - 70, position.height);
		
				// Draw fields - passs GUIContent.none to each so they are drawn without labels
				EditorGUI.PropertyField (minValue, property.FindPropertyRelative ("MinValue"), GUIContent.none);
				EditorGUI.PropertyField (maxValue, property.FindPropertyRelative ("MaxValue"), GUIContent.none);
				EditorGUI.PropertyField (cumulativeDistribution, property.FindPropertyRelative ("CumulativeDistribution"), GUIContent.none);
		
				// Set indent back to what it was
				EditorGUI.indentLevel = indent;
		
				EditorGUI.EndProperty ();
		}
}
#endif