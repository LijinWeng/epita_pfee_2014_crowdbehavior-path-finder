﻿using UnityEngine;
using System.Collections;

public class CameraRTS : MonoBehaviour {

    // For border sliding
    public float moveSpeed = 70;
    private int border = 30;
    
    // For mousewheel zoom
    public float zoomSpeed = 10;
    public int zoomMax = 10;
    public int zoomMin = 50;

    // For camera rotation
    public float rotateSpeed;
    private float mousePositionX;


	// Use this for initialization
	void Start () {
        border = Mathf.Min(Screen.height, Screen.width) / 6;
	}

    void LateUpdate() {

        mousePositionX = Input.mousePosition.x;
    }

	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.LeftControl))
        {

            float mouseX = Input.mousePosition.x;
            float mouseY = Input.mousePosition.y;

            // Sliding part
            if (mouseX > 0 && mouseX < Screen.width && mouseY > 0 && mouseY < Screen.height)
            {

                if (mouseX < border)
                    transform.Translate(Vector3.right * -moveSpeed * Time.deltaTime);

                if (mouseX > Screen.width - border)
                    transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);

                if (mouseY < border)
                    transform.Translate(Vector3.up * -moveSpeed * Time.deltaTime);

                if (mouseY > Screen.height - border)
                    transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
            }

            // Zoom part
            var mouseScrollWheel = Input.GetAxis("Mouse ScrollWheel");
            if (this.camera.orthographicSize > zoomMax && mouseScrollWheel > 0)
                this.camera.orthographicSize -= zoomSpeed * mouseScrollWheel;

            if (this.camera.orthographicSize < zoomMin && mouseScrollWheel < 0)
                this.camera.orthographicSize -= zoomSpeed * mouseScrollWheel;


            // Rotation part
            /*if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButton(1))
            {
                if (mouseX != mousePositionX)
                {
                    var rotation = (mouseX - mousePositionX) * rotateSpeed * Time.deltaTime;
                    this.transform.Rotate(0, rotation, 0);
                }
            }*/
        }
	}
}
